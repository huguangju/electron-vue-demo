import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'archive-list',
      component: require('@/components/Archive/List').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
