export default {
  _id: '',
  fullname: '',
  year: '',
  keepDate: '',
  boxNo: '',
  unitNo: '',
  title: '',
  liablePerson: '',
  time: '',
  pageCount: 0,
  secretLevel: 0,
  remark: '',
  type: 'Qing'
}
